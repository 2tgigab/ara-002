CREATE TABLE `tbluser` (
  `idUser` INT AUTO_INCREMENT NOT NULL,
  `userName` VARCHAR(45) NOT NULL,
  `userPw` VARCHAR(45) NOT NULL,
  `userEmail` VARCHAR(45) NOT NULL,
  `userCargo` ENUM('A','C') NOT NULL,
  PRIMARY KEY (`idUser`))
COMMENT = 'Tabela Utilizador';

CREATE TABLE `tblretalho` (
  `idretalho` INT AUTO_INCREMENT NOT NULL COMMENT 'Id do Produto (PK)',
  `nomeRetalho` VARCHAR(45) NOT NULL COMMENT 'Nome do Produto',
  `descricaoRetalho` VARCHAR(45) NOT NULL COMMENT 'Descrição do produto',
  `precoRetalho` DOUBLE NOT NULL COMMENT 'Preço do Produto',
  `quantidadeRetalho` INT NOT NULL COMMENT 'Quantidade do produto',
  PRIMARY KEY (`idretalho`))
COMMENT = 'Tabela para os retalhos';


SELECT * FROM tbluser;

select * from tblretalho;


/*Insert de admin*/
INSERT INTO tbluser (userName,userPw,userEmail,userCargo) VALUES ('Gabz','123','g@g.com','A');

/*Insert Retalho*/
INSERT INTO tblretalho (nomeRetalho,descricaoRetalho,precoRetalho,quantidadeRetalho) VALUES ('Beyblade','Brinquedo',10.7,300);