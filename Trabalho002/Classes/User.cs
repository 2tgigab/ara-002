﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Trabalho002.Classes
{
	public class User
	{
        protected int idUser;
        protected static int idRecebido;
        protected string nomeRecebido;
        protected string PwRecebido;
    }

    public class CriarUser : User
    {
        public int idUserPub
        {
            get { return idUser; }
            set { idUser = value; }
        }

        public int idRecebidoPub
        {
            get { return idRecebido; }
            set { idRecebido = value; }
        }

        public string nomeRecebidoPub
		{
			get { return nomeRecebido; }
			set { nomeRecebido = value; }
		}

        public string pwRecebidoPub
        {
            get { return PwRecebido; }
            set { PwRecebido = value; }
        }

        //Pegar cargo do ID
        public string cargoID()
        {
            Connection ligar = new Connection();
            string connection = ligar.ligarxampp;
            //string connection = "server=localhost;user id=root;database=dvds,Pwd=tgpsi";
            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection);


            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "K";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userCargo from tbluser WHERE idUser='" + idRecebido + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userCargo"].ToString();
                    return verificar;
                }


                return verificar;



            }

        }

        public bool VerificarUser()
        {
            //Instanciar connection
            Connection connection1 = new Connection();


            string connection = connection1.ligarxampp;
            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection);

            try
            {
                string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
                using (MySqlConnection con = new MySqlConnection(cs))
                {
                    //Abrir conexao
                    con.Open();
                    //Comando para MySQL
                    MySqlCommand cmd = new MySqlCommand("select idUser from tbluser WHERE userName='" + nomeRecebido + "';", con);
                    //Executar Comando
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Ler DataReader
                    dr.Read();

                    idRecebido = int.Parse(dr["idUser"].ToString());


                    return true;
                }
            }
            catch
            {
                return false;
            }


        }

        public bool VerificarSenha()
        {
            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "-!-";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userPw from tbluser WHERE userName='" + nomeRecebido + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userPw"].ToString();
                }


                if (verificar != PwRecebido)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}