﻿using MySql.Data.MySqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Trabalho002.Classes
{
	public class VerificarRegisto
	{
		static string Cod;
		static string statEmail;

		public bool Verificar(string nome, string senha, int idade, string zipCode, string NIF, string sexo, string email)
		{
			//Verificar se o username ja existe na DB
			//Se nao houver, criar conta

			Cod = zipCode;
			statEmail = email;


			//Verificar Nome
			if (nome.Length < 3)
			{
				//Mostrar mensagem de erro que conta ja existe
				return false;
			}
			else
			{


				Regex rgx = new Regex("[^A-Za-z0-9]");
				bool containsSpecialCharacter = rgx.IsMatch(senha);

				if (containsSpecialCharacter && senha.Length > 7)
				{
					//Verificar Idade
					if (idade >= 18)
					{
						//Instanciar objeto de Registo
						Verificar verificar = new Verificar();
						verificar.usernamePub = nome;
						verificar.emailPub = email;
						verificar.cargoPub = 'C';

						//Verificar Email
						if (verificar.IsValidEmail(email) == true)
						{
							if (verificar.VerificarExisteReg() == true)
							{
								//Verificar se email esta em uso
								if (verificar.VerificarExisteEmailReg())
								{
									//Verificar NIF
									if (NIF.Length == 9)
									{
										//Verificar Codigo Postal
										ValidarZIP();



										return true;

									}
									else
									{
										return false;
									}


								}
								else
								{
									return false;
								}



							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}




				}
				else
				{
					return false;
				}

			}





		}
		public bool VerificarExisteEmailReg()
		{
			//Estabelecer conexão com a base de dados
			string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
			using (MySqlConnection con = new MySqlConnection(cs))
			{
				string verificar = "-!-";
				//Abrir conexao
				con.Open();
				//Comando para MySQL
				MySqlCommand cmd = new MySqlCommand("select userEmail from tbluser WHERE userEmail='" + statEmail + "';", con);
				//Executar Comando
				MySqlDataReader dr = cmd.ExecuteReader();
				//Ler DataReader
				if (dr.Read())
				{
					verificar = dr["userEmail"].ToString();
				}


				if (verificar != statEmail)
				{
					return true;
				}
				else
				{
					return false;
				}

			}


		}

		protected string ValidarZIP()
		{
			string zip = Cod;

			zip = zip.Substring(4, 1);


			if (zip != "-")
			{
				return zip;

			}
			else
			{
				return zip;

			}

		}
	}
}