﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Trabalho002.Classes
{
	public class BaseDadosRegisto
	{
        protected string username;
        protected string pw;
        protected string id;
        protected string email;
        protected char cargo;

    }

    public class Verificar : BaseDadosRegisto
    {
        public string usernamePub
        {
            get { return username; }
            set { username = value; }
        }

        public string pwPub
        {
            get { return pw; }
            set { pw = value; }
        }

        public string idPub
        {
            get { return id; }
            set { id = value; }
        }

        public string emailPub
        {
            get { return email; }
            set { email = value; }
        }

        public char cargoPub
        {
            get { return cargo; }
            set { cargo = value; }
        }



        public bool VerificarUser()
        {

            Connection connection = new Connection();
            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection.ligar);

            try
            {
                string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
                using (MySqlConnection con = new MySqlConnection(cs))
                {
                    //Abrir conexao
                    con.Open();
                    //Comando para MySQL
                    MySqlCommand cmd = new MySqlCommand("select idUser from tbluser WHERE userName='" + username + "';", con);
                    //Executar Comando
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Ler DataReader
                    dr.Read();

                    id = dr["idUser"].ToString();


                    return true;
                }
            }
            catch
            {
                return false;
            }


        }

        public bool VerificarSenha()
        {
            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "-!-";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userPw from tbluser WHERE userName='" + username + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userPw"].ToString();
                }


                if (verificar != pw)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region Registo


        public bool VerificarExisteReg()
        {
            Connection connection = new Connection();
            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection.ligar);


            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "-!-";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userName from tbluser WHERE userName='" + username + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userName"].ToString();
                }


                if (verificar != username)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }


        }


        public bool VerificarExisteEmailReg()
        {
            Connection connection = new Connection();
            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection.ligar);


            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "-!-";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userEmail from tbluser WHERE userEmail='" + email + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userEmail"].ToString();
                }


                if (verificar != email)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }


        }

        public bool IsValidEmail(string email)
        {
            var trimmedEmail = email.Trim();

            if (!trimmedEmail.EndsWith(".com"))
            {
                return false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }

        #endregion


        public string PegarCargoID()
        {
            Connection connection = new Connection();

            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection.ligar);


            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                string verificar = "K";
                //Abrir conexao
                con.Open();
                //Comando para MySQL
                MySqlCommand cmd = new MySqlCommand("select userCargo from tbluser WHERE userName='" + username + "';", con);
                //Executar Comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Ler DataReader
                if (dr.Read())
                {
                    verificar = dr["userCargo"].ToString();
                    return verificar;
                }


                return verificar;



            }
        }


    }
}
