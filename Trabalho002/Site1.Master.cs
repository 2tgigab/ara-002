﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trabalho002.Classes;

namespace Trabalho002
{
	public partial class Site1 : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			btnAdmin.Visible = false;
			btnSair.Visible = false;



			//Verificar se é admin ou nao
			CriarUser user = new CriarUser();

			if(user.idRecebidoPub == 0)
			{
				btnAdmin.Visible = false;
			}
			else
			{
				//Retriar botao de login e registo
				btnRegisto.Visible = false;
				btnLogin.Visible = false;
				btnSair.Visible = true;


				//Se for A é admin se for C é cliente
				if (user.cargoID() == "C")
				{
					//Colocar botao admin invisivel
					btnAdmin.Visible = false;
				}
				else
				{
					btnAdmin.Visible = true;
				}
			}

			
		}

		protected void btnHome_Click(object sender, EventArgs e)
		{
			Response.Redirect("Home.aspx");
		}

		protected void btnRegisto_Click(object sender, EventArgs e)
		{
			Response.Redirect("Registo.aspx");
		}

		protected void btnSair_Click(object sender, EventArgs e)
		{
			//Instanciar user para resetar ID
			CriarUser user = new CriarUser();
			user.idRecebidoPub = 0;
			Response.Redirect("Home.aspx");
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			Response.Redirect("Login.aspx");
		}

		protected void btnRetalhos_Click(object sender, EventArgs e)
		{
			Response.Redirect("Retalhos.aspx");
		}
	}
}