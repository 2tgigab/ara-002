﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trabalho002
{
    public partial class Registo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Instanciar Objeto User
            Classes.CriarUser user = new Classes.CriarUser();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Classes.VerificarRegisto registo = new Classes.VerificarRegisto();
            if (registo.Verificar(txbUsername.Text, txbSenha.Text, CalcularIdade(), txbZip.Text, txbNif.Text, Sexos.SelectedValue.ToString(), txbEmail.Text) == true)
            {
                //Criar Conta
                CriarConta();
                Response.Redirect("Login.aspx");
            }
            else
            {
                Response.Write("ERRO!");
            }

        }

        protected void CriarConta()
        {
            Classes.Connection connection = new Classes.Connection();

            //Estabelecer conexão com a base de dados
            MySqlConnection mySqlConnection = new MySqlConnection(connection.ligar);

            try
            {
                string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
                using (MySqlConnection con = new MySqlConnection(cs))
                {
                    //Abrir conexao
                    con.Open();
                    //Comando para MySQL
                    MySqlCommand cmd = new MySqlCommand("INSERT INTO tbluser(userName,userPw,userEmail,userCargo) VALUES ('" + txbUsername.Text + "','" + txbSenha.Text + "','" + txbEmail.Text + "','C');", con);
                    //Executar Comando
                    cmd.ExecuteReader();

                }
            }
            catch
            {

            }
        }

        protected int CalcularIdade()
        {
            try
            {
                string diaNasc = txbNasc.Text;


                diaNasc = diaNasc.Substring(0, 4);

                DateTime today = DateTime.Today;
                int age = today.Year - int.Parse(diaNasc);

                return age;
            }
            catch
            {
                return 0;
            }
        }
    }
}