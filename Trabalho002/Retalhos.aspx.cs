﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trabalho002
{
	public partial class Retalhos : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//Meter botao de funcionario invisivel

				BaseDados();


			}
			//////


		}

        protected void BaseDados()
        {
            
            string cs = ConfigurationManager.ConnectionStrings["Trabalho002"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(cs))
            {
                //Abrir conexão
                con.Open();
                //Código MySql Para selecionar informação da tabela encomendas
                MySqlCommand cmd = new MySqlCommand("select idretalho, nomeRetalho, precoRetalho, quantidadeRetalho from tblretalho", con);
                //Executar o comando
                MySqlDataReader dr = cmd.ExecuteReader();
                //Preencher a GridView
                if (dr.HasRows == true)
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                    //Fechar Conexão
                    con.Close();
                }
            }
        }

    }
}