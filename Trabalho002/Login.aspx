﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Trabalho002.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<div>
        <br />
        <h1>Login</h1>
        <asp:Label ID="lblUsername" runat="server" Text="Username: "></asp:Label>
        <asp:TextBox ID="txbUsername" runat="server" placeholder ="Insira Username"></asp:TextBox>
        <br />
        <asp:Label ID="lblSenha" runat="server" Text="Password: "></asp:Label>
        <asp:TextBox ID="txbSenha" runat="server" placeholder ="Insira Senha"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="button" OnClick="btnLogin_Click" />
        <br />

    </div>


</asp:Content>
