﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Registo.aspx.cs" Inherits="Trabalho002.Registo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="card-register">
        <br />
        <h1>Registo</h1>
    
        <br />
        <table class="auto-style3">
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblUsername" runat="server" Text="Username:"></asp:Label>
                </td>
                <td>
        <asp:TextBox ID="txbUsername" runat="server" placeholder ="Insira Username"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
                </td>
                <td>
        <asp:TextBox ID="txbEmail" runat="server" placeholder ="Insira Email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblSenha" runat="server" Text="Password:"></asp:Label>
                </td>
                <td>
        <asp:TextBox ID="txbSenha" runat="server" placeholder ="Insira Senha" ToolTip="8 Caracteres e um especial" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Nascimento" runat="server" Text="Data de Nascimento:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txbNasc" runat="server"  placeholder="Insira Senha" TextMode="Date" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2"><asp:Label ID="lblZIP" runat="server" Text="Cod.Postal:"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txbZip" runat="server"   placeholder="Insira Cod-Postal" ToolTip ="0000-000"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>

                    <asp:Label ID="lblNif" runat="server" Text="NIF:"></asp:Label>

                </td>

                <td>
                    <asp:TextBox ID="txbNif" runat="server" TextMode="Number" placeholder="Insira o NIF" ToolTip="9 caracteres"></asp:TextBox>
                </td>

            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblGenero" runat="server" Text="Género:"></asp:Label>
                </td>

                <td>
                    <asp:DropDownList ID="Sexos" runat="server">
                        <asp:ListItem>Masculino</asp:ListItem>
                        <asp:ListItem>Feminino</asp:ListItem>
                        <asp:ListItem>Outro</asp:ListItem>
                    </asp:DropDownList>

                </td>

            </tr>

           


        
        </table>
        <asp:Button ID="btnLogin" runat="server" CssClass="button" Text="Registar" OnClick="btnLogin_Click" />

        <br />

    </div>
</asp:Content>
